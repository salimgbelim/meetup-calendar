import FeaturedMeetup from '@/components/FeaturedMeetup';
import { shallow } from 'vue-test-utils';

describe('FeaturedMeetup.vue', () => {
  const card = {
    'name': 'Kanban for ITSM',
    'time': 1511202600000,
    'local_date': '2017-11-20',
    'local_time': '18:30',
    'yes_rsvp_count': 109,
    'venue': {
      'name': 'Valtech',
      'address_1': 'Basil Chambers. 65 High Street. M4 1FS'
    }
  };

  it('should show featured meetup', () => {
    const cmp = shallow(FeaturedMeetup, {
      propsData: {
        cards: [card]
      }
    });
    expect(cmp.find('.featured-meetup__title').text()).to.equal(card.name);
    expect(cmp.find('.featured-meetup__rsvp').text()).to.contain(card.yes_rsvp_count);
    expect(cmp.find('.featured-meetup__day').text()).to.contain(card.time).and.contain(card.local_date);
    expect(cmp.find('.featured-meetup__time').text()).to.equal(card.local_time);
    expect(cmp.find('.featured-meetup__venue').text()).to.equal(card.venue.name);
    expect(cmp.find('.featured-meetup__address').text()).to.equal(card.venue.address_1);
  });
});
