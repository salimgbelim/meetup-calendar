var express = require('express');
var path = require('path');
var serveStatic = require('serve-static');
var axios = require('axios');
app = express();
app.use(serveStatic(__dirname + "/dist"));
var port = process.env.PORT || 8080;
app.listen(port);
console.log('server started '+ port);

// meetup API
var instance = axios.create({
  baseURL: 'https://api.meetup.com/'
});

app.get('/meetupAPI', function(req, res) {
  const apiKey = '597035261b41a24214f273a2953357d';
  const isSigned = 'true';
  const photoHost = 'public';
  const pageCount = '20';
  const url = '/self/calendar?' + 'key=' + apiKey + '&sign=' + isSigned + '&photo-host=' + photoHost + '&page=' + pageCount + '';

  instance.get(url)
  .then(response => {
    var result = [];
    for (var i = 0; i < response.data.length; i++) {
      if (i > 0 && response.data[i].local_date === response.data[i - 1].local_date) {
        result.push(response.data[i]);
        result.push(response.data[i - 1]);

        response.data[i - 1] = result;
        response.data.splice([i], 1);
        result = [];
        i--;
      }

      if (i > 0 && response.data[i - 1].length) {
        if (response.data[i].local_date === response.data[i - 1][0].local_date) {
          result.push(response.data[i]);

          for (j = 0; j < response.data[i - 1].length; j++) {
            result.push(response.data[i - 1][j]);
          }

          response.data[i - 1] = result;
          response.data.splice([i], 1);
          result = [];
          i--;
        }
      }
    }

    return res.send(response.data);
  })
  .catch(e => {
    return e;
  })
});
